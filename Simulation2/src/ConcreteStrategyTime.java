import java.util.ArrayList;

public class ConcreteStrategyTime implements Strategy {

	public void addClient(ArrayList<Server> servers, Client client,Integer maxClientsPerServer) {
		int min=Integer.MAX_VALUE;
		Server s = new Server();
		for(Server index: servers){
			if(index.getWaitingTime()<min && index.getNoClients()<maxClientsPerServer){
				min=index.getWaitingTime();
				s=index;
			}
		}
		int time;
		client.setFinishTime(s.getWaitingTime()+client.getProcessingTime()+client.getArrivalTime());
		time=client.getProcessingTime()+s.getWaitingTime();
		s.setWaitingTime(time);
		s.addClientToServer(client);
		//System.out.println("clients size: "+s.getNoClients());
	}

}

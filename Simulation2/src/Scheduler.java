import java.util.ArrayList;


public class Scheduler {
	
	private ArrayList<Server> servers;
	private Integer maxNoServers;
	private Integer maxClientsPerServer;
	private Strategy strategy;
	
	public Scheduler(int maxNS, int maxCS){
		this.maxNoServers= maxNS;
		this.maxClientsPerServer=maxCS;
		servers= new ArrayList<Server>();
		int i;
		for(i=0;i<maxNoServers;i++){
			Server s= new Server(maxCS);
			servers.add(s);
			Thread t= new Thread(s);
			t.setName("i"+i);
			t.start();
		}
	}
	
	public void dispachClient(Client client){
		strategy.addClient(servers, client,maxClientsPerServer);
	}
	
	
	public void changeStrategy(SelectionPolicy policy){
		if(policy == SelectionPolicy.SHORTEST_QUEUE){
			strategy= new ConcreteStrategyQueue();
		}
		if(policy == SelectionPolicy.SHORTEST_TIME){
			strategy = new ConcreteStrategyTime();
		}
	}
	
	
	public int getMaxNoServers() {
		return maxNoServers;
	}
	
	public int getMaxClientsPerServer() {
		return maxClientsPerServer;
	}
	
	public ArrayList<Server> getServers() {
		return servers;
	}
	
}


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimulationFrame extends JFrame {

	private static final long serialVersionUID = -8107530202665533044L;
	public static JTextField q1;
	public static JTextField q2;
	public static JTextField q3;
	public static JTextArea log;
	
	
	public SimulationFrame(){
		setTitle("Simulation");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 450);
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		init();
	}
	
	public void init(){
		JTextField timeT;
		JTextField numberCST;
		JTextField numberClientT;
		JRadioButton strTime;
		JRadioButton strQueue;
		
		JPanel queues;
		JPanel bigPanel= new JPanel();
		bigPanel.setBackground(new Color(245,222,179));
		
		JLabel timeL;
		JLabel numberCSL;
		JLabel numberClientL;
		JLabel strategyL;
		
		JPanel panelLabel= new JPanel();
		JPanel panelText= new JPanel();
		
		panelLabel.setBackground(new Color(245,222,179));
		panelLabel.setLayout(new BoxLayout(panelLabel, BoxLayout.PAGE_AXIS));
		timeL= new JLabel();
		numberCSL= new JLabel();
		numberClientL=new JLabel();
		timeL.setText("Time Limit:");
		numberCSL.setText("Number of clients per server:");
		numberClientL.setText("Number of clients:");
		panelLabel.add(timeL);
		panelLabel.add(numberCSL);
		panelLabel.add(numberClientL);
		panelLabel.setVisible(true);
		bigPanel.add(panelLabel);
		
		
		panelText.setBackground(new Color(245,222,179));
		panelText.setLayout(new BoxLayout(panelText, BoxLayout.PAGE_AXIS));
		timeT= new JTextField();
		numberCST= new JTextField();
		numberClientT = new JTextField();
		timeT.setColumns(5);
		numberCST.setColumns(5);
		numberClientT.setColumns(5);
		panelText.add(timeT);
		panelText.add(numberCST);
		panelText.add(numberClientT);
		panelText.setVisible(true);
		bigPanel.add(panelText);
		
		JPanel panelStrategy = new JPanel();
		panelStrategy.setBackground(new Color(245,222,179));
		panelStrategy.setLayout(new BoxLayout(panelStrategy,BoxLayout.PAGE_AXIS));
		
		strategyL= new JLabel();
		strategyL.setText("   Choose strategy:");
		strTime= new JRadioButton();
		strQueue= new JRadioButton();
		strTime.setText("Shortest Time");
		strTime.setBackground(new Color(245,222,179));
		strQueue.setText("Shortest Queue");
		strQueue.setBackground(new Color(245,222,179));
		panelStrategy.add(strategyL);
		panelStrategy.add(strTime);
		panelStrategy.add(strQueue);
		bigPanel.add(panelStrategy);
		
		strTime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(strTime.isSelected())
					strQueue.setSelected(false);
			}});
		
		strQueue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(strQueue.isSelected())
					strTime.setSelected(false);
			}});
		
		JLabel img;
		ImageIcon calcImg= new ImageIcon("people.png");
		img= new JLabel(calcImg);
		bigPanel.add(img);
		
		queues= new JPanel();
		queues.setLayout(new BoxLayout(queues,BoxLayout.PAGE_AXIS));
		q1 = new JTextField();
		q1.setColumns(40);
		q2 = new JTextField();
		q2.setColumns(40);
		q3 = new JTextField();
		q3.setColumns(40);
		q1.setFont(new Font(q1.getFont().getName(),Font.PLAIN, 14));
		q2.setFont(new Font(q2.getFont().getName(),Font.PLAIN, 14));
		q3.setFont(new Font(q3.getFont().getName(),Font.PLAIN, 14));
		queues.add(q1);
		queues.add(q2);
		queues.add(q3);
		queues.setVisible(true);
		bigPanel.add(queues);
		
		JPanel panelLog = new JPanel();
		panelLog.setBackground(new Color(245,222,179));
		panelLog.setLayout(new BoxLayout(panelLog,BoxLayout.PAGE_AXIS));
		log= new JTextArea(10,40);
		log.setText("");
		log.setVisible(true);
		JScrollPane scrLog= new JScrollPane(log);
		panelLog.add(scrLog);
		bigPanel.add(panelLog);
		
		JButton start = new JButton("Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer time=Integer.parseInt(timeT.getText());
				Integer clperS= Integer.parseInt(numberCST.getText());
				Integer clients= Integer.parseInt(numberClientT.getText());
				log.setText("");
				SelectionPolicy sp=SelectionPolicy.SHORTEST_TIME;
				if(strTime.isSelected())
					sp=SelectionPolicy.SHORTEST_TIME;
				else
					if(strQueue.isSelected())
						sp=SelectionPolicy.SHORTEST_QUEUE;
				SimulationManager sm= new SimulationManager(time,clperS,clients,sp);
				Thread t= new Thread(sm);
				t.start();
			}
		});
		bigPanel.add(start);
		
		bigPanel.setVisible(true);
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	
	
	public static void main(String[] args){
		new SimulationFrame();
	}
}

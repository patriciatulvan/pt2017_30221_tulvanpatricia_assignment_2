
public class Client implements Comparable<Client> {
	
	private Integer arrivalTime;
	private Integer processingTime;
	private Integer finishTime;
	private Integer clientNumber;
	
	public Client(){
		arrivalTime=0;
		processingTime=0;
		finishTime=0;
	}
	
	public Client(Integer arrivalTime, Integer processingTime){
		this.arrivalTime=arrivalTime;
		this.processingTime=processingTime;
		this.finishTime=0;
	}
	
	public Integer getArrivalTime() {
		return arrivalTime;
	}
	
	public Integer getProcessingTime() {
		return processingTime;
	}
	
	public void setFinishTime(Integer finishTime) {
		this.finishTime = finishTime;
	}
	
	public void setClientNumber(Integer clientNumber) {
		this.clientNumber = clientNumber;
	}
	
	public Integer getClientNumber() {
		return clientNumber;
	}
	
	public Integer getFinishTime() {
		return finishTime;
	}

	@Override
	public int compareTo(Client c) {
		return arrivalTime.compareTo(c.arrivalTime);
	}

}

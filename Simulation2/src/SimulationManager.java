import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class SimulationManager implements Runnable{
	
	public boolean end=false;
	public Integer timeLimit;
	public Integer maxProcessingTime=10;
	public Integer minProcessingTime=2;
	public Integer numberOfServers=3;
	public Integer numberOfCS;
	public Integer numberOfClients;
	public SelectionPolicy selectionPolicy;
	
	private Scheduler scheduler;
	private volatile ArrayList<Client> generatedClients;
	private int currentTime;
	private Integer peekMax=Integer.MIN_VALUE;
	private int peek;

	
	public SimulationManager(Integer timeLimit, Integer numberOfCS, Integer numberOfClients,SelectionPolicy selectionPolicy){
		this.timeLimit=timeLimit;
		this.numberOfCS=numberOfCS;
		this.numberOfClients=numberOfClients;
		this.selectionPolicy=selectionPolicy;
		scheduler= new Scheduler(numberOfServers,numberOfCS);
		scheduler.changeStrategy(selectionPolicy);
		generatedClients= new ArrayList<Client>();
		generateNRandomClients();
	}
	
	public void generateNRandomClients(){
		int processingTime;
		int arrivalTime;
		Random random= new Random();
		for(int i=0;i<numberOfClients;i++){
			processingTime=random.nextInt(maxProcessingTime-minProcessingTime)+minProcessingTime;
			arrivalTime=random.nextInt(timeLimit);
			generatedClients.add(new Client(arrivalTime,processingTime));
		}
		sort();
		for(int i=0; i<generatedClients.size(); i++){
			generatedClients.get(i).setClientNumber(i+1);
		}
	}
	
	public void sort(){
		Collections.sort(generatedClients);
	}
	
	@Override
	public void run() {
		Client client= new Client();
		Integer avg=0;
		currentTime=0;
		while(currentTime<timeLimit ){
			int i;
			for(i=0;i<generatedClients.size();i++){
					client= generatedClients.get(i);
					if(client.getArrivalTime()==currentTime && client.getFinishTime()==0){
						scheduler.dispachClient(client);
						generatedClients.remove(client);
						i--;
						update(scheduler);
					}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			avg+=averageWaitingTime();
			peekTime(currentTime);
			currentTime++;
			update(scheduler);
			//System.out.println(currentTime);
		}
		while(scheduler.getServers().get(0).getClients().size()!=0 || scheduler.getServers().get(1).getClients().size()!=0 || scheduler.getServers().get(2).getClients().size()!=0 ){
			update(scheduler);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			avg+=averageWaitingTime();
			peekTime(currentTime);
			currentTime++;
		}
		SimulationFrame.q1.setText("");
		SimulationFrame.q2.setText("");
		SimulationFrame.q3.setText("");
		SimulationFrame.log.setText(SimulationFrame.log.getText()+"Average waiting time: "+avg/numberOfClients + " sec\n");
		SimulationFrame.log.setText(SimulationFrame.log.getText()+"Peek time at second: "+peek +"\n");
	}

	public static void update(Scheduler s){
		SimulationFrame.q1.setText("");
		String x="";
		if(s.getServers().get(0).getClients().size()!=0)
			for(int j=0;j< s.getServers().get(0).getClients().size();j++){
				x=x+'\u263B';
			}
		SimulationFrame.q1.setText(x);
		
		SimulationFrame.q2.setText("");
		String x2="";
		if(s.getServers().get(1).getClients().size()!=0)
			for(int j=0; j<s.getServers().get(1).getClients().size();j++){
				x2=x2+'\u263B';
			}
		SimulationFrame.q2.setText(x2);
	
		SimulationFrame.q3.setText("");
		String x3="";
		if(s.getServers().get(2).getClients().size()!=0)
			for(int j=0;j< s.getServers().get(2).getClients().size();j++){
				x3=x3+'\u263B';
			}
		SimulationFrame.q3.setText(x3);
	}
	
	public Integer averageWaitingTime(){
		Integer avgWT=0;
		for(int i=0; i<scheduler.getServers().size();i++){
			Server s= scheduler.getServers().get(i);
			avgWT+=s.getWaitingTime();
		}
		return avgWT/scheduler.getMaxNoServers();
	}
	
	public void peekTime(int currentTime){
		Integer x=0;
		for(int i=0; i<scheduler.getServers().size();i++){
			Server s= scheduler.getServers().get(i);
			x+=s.getNoClients();
		}
		if(x>peekMax){
			peekMax=x;
			peek=currentTime;
		}
	}

}

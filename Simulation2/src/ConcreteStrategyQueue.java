
import java.util.ArrayList;

public class ConcreteStrategyQueue implements Strategy{

	public void addClient(ArrayList<Server> servers, Client client,Integer maxClientsPerServer) {
		int min=Integer.MAX_VALUE;
		Server s = new Server();
		for(Server index : servers){
			if(index.getNoClients()<min && index.getNoClients()<maxClientsPerServer){
				min=index.getNoClients();
				s=index;
			}
		}
		int time;
		client.setFinishTime(s.getWaitingTime()+client.getProcessingTime()+client.getArrivalTime());
		time=client.getProcessingTime()+s.getWaitingTime();
		s.setWaitingTime(time);
		s.addClientToServer(client);
		
	}

}
